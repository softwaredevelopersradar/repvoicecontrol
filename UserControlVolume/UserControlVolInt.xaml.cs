﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace UserControlVolume
{
    /// <summary>
    /// Логика взаимодействия для UserControlVolInt.xaml
    /// </summary>
    public partial class UserControlVolInt : UserControl
    {
        #region delegate
        public delegate void MyControlEventHandler();
        public event MyControlEventHandler OnTakeFileCheck;
        public event MyControlEventHandler OnTakeOnlineCheck;
        public event MyControlEventHandler OnButPlayMouseDown;
        public event MyControlEventHandler OnButPlayMouseUp;
        public event MyControlEventHandler OnButOpenFile;

        public delegate void TextChangedEventHandler(object sender, TextCompositionEventArgs e);
        public event TextChangedEventHandler OnTextInput;

        public delegate void SelectionChangedEventHandler();
        public event SelectionChangedEventHandler OnComboboxMode;

        public event EventHandler<DeviationEventArgs> OnGetDeviation;
        #endregion

        OpenFileDialog open = null;

        string fileName;
        public string FileName
        {
            get => fileName; 
            set { }
        }

        byte deviation;
        public byte Deviation
        {
            get => deviation;
            set { }
        }
        double Freq = 117.500;
        public double Frequency
        {
            get => Freq;
            set { }
        }
        byte takeFileIndex;
        public byte TakeFileIndex
        {
            get => takeFileIndex;
            set { }
        }

        public UserControlVolInt()
        {
            InitializeComponent();
            TakeFile.IsChecked = true;
            takeFileIndex = 0;
            Zopolnenie_Combobox();
            cbDeviation.SelectedIndex = 0;
        }

        private void OpenFile_Click(object sender, RoutedEventArgs e)
        {
            OnButOpenFile?.Invoke();
            open = new OpenFileDialog
            {
                Filter = "Wave File (*.wav)|*.wav;"
            };
            if (open.ShowDialog() != true) return;
            FileSource.Text = open.FileName;
            fileName = open.FileName;
        }

        private void ButPlay_PreviewMouseDown(object sender, MouseButtonEventArgs e)
        {
            if (double.Parse(TboxFreq.Text) < 30 || double.Parse(TboxFreq.Text) > 1215)
            {
                MessageBox.Show("Incorrectly frequency");
            }
            else 
            {
                double.TryParse(TboxFreq.Text, out Freq);
                Keyboard.ClearFocus();
                OnButPlayMouseDown?.Invoke();
                VolumePlay.Visibility = Visibility.Visible;
            }            
        }

        private void ButPlay_PreviewMouseUp(object sender, MouseButtonEventArgs e)
        {
            OnButPlayMouseUp?.Invoke();
            VolumePlay.Visibility = Visibility.Collapsed;
        }

        private void TakeOnline_Checked(object sender, RoutedEventArgs e)
        {
            FileSource.IsEnabled = false;
            OpenFile.IsEnabled = false;
            takeFileIndex = 1;
            OnTakeOnlineCheck?.Invoke();
        }

        private void TakeFile_Checked(object sender, RoutedEventArgs e)
        {            
            FileSource.IsEnabled = true;
            OpenFile.IsEnabled = true;
            takeFileIndex = 0;
            OnTakeFileCheck?.Invoke();
        }

        private void Zopolnenie_Combobox()
        {
            List<string> frq = new List<string>() { "3", "6", "12", "100" };
            cbDeviation.ItemsSource = frq;
        }

        private void TboxFreq_KeyDown(object sender, KeyEventArgs e)
        {
            
            if (e.Key == Key.Enter)
            {
                double.TryParse(TboxFreq.Text, out Freq);
                Keyboard.ClearFocus();
            }
            
            //if(e.Key == Key.)
        }

        private void TboxFreq_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            OnTextInput?.Invoke(this, e);
            if (!(Char.IsDigit(e.Text, 0) || (e.Text == ",")
               && (!TboxFreq.Text.Contains(",")
               && TboxFreq.Text.Length != 0)))
            {
                e.Handled = true;
                //Keyboard.ClearFocus();
            }            
        }

        private void cbDeviation_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            //OnComboboxMode?.Invoke();
            switch (cbDeviation.SelectedIndex)
            {
                case 0: deviation = 1; break;  //3
                case 1: deviation = 2; break;  //6
                case 2: deviation = 3; break;  //12
                case 3: deviation = 4; break;  //100
            }
            OnComboboxMode?.Invoke();
            OnGetDeviation?.Invoke(this, new DeviationEventArgs(deviation));
        }

        private void TboxFreq_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Space) 
            {
                e.Handled = true;
            }
        }
    }
}
