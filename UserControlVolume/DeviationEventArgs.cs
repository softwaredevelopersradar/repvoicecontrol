﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UserControlVolume
{
    public class DeviationEventArgs : EventArgs
    {
        public DeviationEventArgs(Int16 Dev)
        {
            Deviation = Dev;
        }

        public Int16 Deviation { get; private set; }
    }
}
